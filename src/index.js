import { createStore } from 'redux';
import React from 'react';
import ReactDOM from 'react-dom';
// import App from './App';
import Counter from './Counter';
import './index.css';

const counter = (state = 0, action) => {
  switch (action.type) {
    case 'INCREMENT':
      return state + 1;
    case 'DECREMENT':
      return state - 1;
    default:
      return state;
  }
}

// const { createStore } = Redux;
const store = createStore(counter);

const render = () => {
  ReactDOM.render(
    <Counter 
      value={store.getState()} 
      onIncrement={() => 
        store.dispatch({
          type: 'INCREMENT'
        })
      }
      onDecrement={() => 
        store.dispatch({
          type: 'DECREMENT'
        })
      }     
    />,
    document.getElementById('root')
  );
};

store.subscribe(render);
render();

// ReactDOM.render(<h1>Dale!</h1>,document.getElementById('root'));